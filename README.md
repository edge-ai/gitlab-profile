# Edge-AI

Code and documentation to help use TI processors to perform deep learning tasks efficiently

## Objectives

1) Obtain example of building the Edge-AI ARM-side stack without any pre-built ARM binaries
2) Perform end-to-end build using Debian compilers and standard C library
3) Minimize long-term support effort by enabing other maintainers
4) Enable sustainable differentiation using TI DSP capability to the fullest extent

## Need

We are making [BeagleY-AI](https://beagley-ai.org) available to thousands of developers on June 1, 2024
and the AI algorithm performance is a key differentiator to Raspberry Pi 5. We need to be able to demonstrate
this function in the [multitude of OS images](http://downloads.raspberrypi.org/os_list_imagingutility_v4.json)
available for running on Raspberry Pi, otherwise the performance of the AM67A TI processor will look bad.

## Milestones

- [ ] [Rebuild ARM code](https://openbeagle.org/groups/edge-ai/-/milestones/1) by May 10
- [ ] [Package Edge-AI code in Debian](https://openbeagle.org/groups/edge-ai/-/milestones/2) by June 1
- [ ] [Enable coprocessor improvements](https://openbeagle.org/groups/edge-ai/-/milestones/3) by July 1
- [ ] [Push code into upstream projects](https://openbeagle.org/groups/edge-ai/-/milestones/4) by September 1

## Status

* See https://openbeagle.org/groups/edge-ai/-/boards

## Learn more

* https://edge-ai.beagleboard.io/
